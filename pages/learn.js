import Link from 'next/link'
import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Learn() {
    return (
        <>
            <Head>
                <title>Learn and do more</title>
            </Head>
            <section>
                <h1>
                    Work related
                </h1>
                <details>
                    <summary>These are some of my goals for 2022</summary>
                    <p> Technology is consntantly evolving so this is forever learning for me.</p>
                    <p> How to grow proffessionaly within a team. </p>
                    <p> Try  another scripting languge apart from Ruby.</p>
                    <p>Complete atleast 2 technical certifications within this year.</p>
                    <p>Do more: Lead by example and leave an positive impact on the team.</p>
                    <p>Do more: Live by the value of transparency and collaboration </p>
                </details>
            </section>

            <section>
                <h1>
                    Non Work related
                </h1>
                <details>
                    <summary>Feel free to ignore ;)</summary>
                    <p> Work more towards fitness.</p>
                    <p> Mantaining a healthy work life balance.</p>
                    <p> Learn any one music instrument.</p>
                    <p> Read atleast 10 books this year. </p>
                    <p> Improve Linuistics skiils.</p>
                    <p>Do something impactful for the society</p>
                </details>
                <p>
                    <Link href="/" className={styles.butt}>
                        <a>Home</a>
                    </Link>
                </p>
            </section>

        </>
    )
}
