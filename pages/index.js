import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'

import styles from '../styles/Home.module.css'


export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Hello World</title>
        <meta name="description" content="Created by arihant usin next.js" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Hello World
        </h1>

        <p className={styles.description}>
          Get started by clicking any of the {"C2A's"}
        </p>

        <div className={styles.grid}>

          <Link href="tasks">
            <a className={styles.card}><h2>Tasks &rarr;</h2>
              <p> List of my favorite and least favourite tasks.</p>
            </a>
          </Link>

          <Link href="learn">
            <a className={styles.card}>
              <h2>Carrer Path &rarr;</h2>
              <p>What do you want to learn, or do more of, at work? </p>
            </a>
          </Link>

          <Link href="customer_scenarios">
            <a className={styles.card}>
              <h2>Customer Scenarios &rarr;</h2>
              <p>This cover both the customer scenarios</p>
            </a>
          </Link>

          <Link href="articles">
            <a className={styles.card}>
              <h2>Books/articles &rarr;</h2>
              <p>The books and the articles that has left a positive impression on me</p>
            </a>
          </Link>
          <Link href="common_problem">
            <a className={styles.card}>
              <h2>Common Problems &rarr;</h2>
              <p>Common problems which customers ask Vercel for help with</p>
            </a>
          </Link>

          <a href="improvements" className={styles.card}>
            <h2>Improvements &rarr;</h2>
            <p>How could we improve or alter this familiarisation exercise?</p>
          </a>
          <p><code className={styles.code}>Temporary in-app redirect is created for the path /hello-vercel</code></p>


        </div>
      </main>

      <footer className={styles.footer}>
        <a href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"          target="_blank"          rel="noopener noreferrer"        >
          Powered by{' '}
          <span className={styles.logo}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer>
    </div>
  )
}
