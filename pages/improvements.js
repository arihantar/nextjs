import Link from 'next/link'
import Head from 'next/head'

export default function Improvements() {
    return (
        <>
            <Head>
                <title>Improvements</title>
            </Head>
            <section>
                <h1>
                    Improvements
                </h1>
                <ul>
                    <li><p>First I would like to thank you for introducing me to this framework. I really like the problem this framework is solving.</p></li>
                    <li><p>There are few areas of improvements I would like to suggest.</p></li>
                    <ul>
                        <li><p>This can go more towards common problem solving. Not necessary related to the framework. As I believe no one expects a candidate to know the framework before hand.</p></li>
                        <li><p>A simple example would be to provide a project containing a common bug behavior and ask the candidate to fix it in the project.</p></li>
                        <li><p>{"Explain what that bug is and it's root cause."}</p></li>
                        <li><p>For me the pain point was not the exercise. I somehow messed up my local environment while working on a different task earlier and that took a lot of my time to fix. But I am glad that this happened because I now know what not to do.</p></li>
                    </ul>

                </ul>
                <p>
                    <Link href="/" classname="butt">
                        <a>Back to Home</a>
                    </Link>
                </p>

            </section>

        </>
    )
}
