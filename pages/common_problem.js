import Link from 'next/link'
import Head from 'next/head'

export default function CommonProblems() {
    return (
        <>
            <Head>
                <title>Common Problem</title>
            </Head>
            <section>
                <h1>Common Problems</h1>
                <details>
                    <summary> Release Issues/bugs</summary>
                    <p>{"In such cases I'll update the team about the existing issue."}</p>
                    <p>{"Create a macro for the team to use in responding such tickets"}</p>
                    <p>{"Work with developers to suggest a workaround if available."}</p>
                    <p>{"Update all the stakeholders for awareness."}</p>
                    <p>{"Be as much transparent so customers don't loose faith in the product."}</p>
                </details>
                <details>
                    <summary>Customer asking for a certain feature that {"doesn't"} exist.</summary>
                    <p>{"As a support engineer you should work as a customer advocate."}</p>
                    <p>{"Create a feature request for developers to check and make developers aware that a paying customer is asking for this feature."}</p>
                    <p>{"Work with developers to suggest a workaround if any."}</p>
                    <p>{"Update all the stakeholders time to time."}</p>
                </details>

                <details>
                    <summary> How to questions</summary>
                    <p>{"Our documentation should be precised enough to cover all the how to's including common problems customer might face. For example related to any integerations."}</p>
                    <p>{"Updating documentation should always be an ongoing process."}</p>
                    <p>{"Create videos/blog posts about a particular feature that you think worth creating and save so many support hours."}</p>
                    <p>{"Tickets are the best place to identify this data."}</p>
                </details>
                <details>
                    <summary>Angry customers</summary>
                    <p>{"There may be cases where a customer will simply be unsatisfied with all available solutions and/or steps taken to solve problems they are facing or have faced. De-escalate the situation the best you can - apologize as necessary, offer solutions or alternatives that may work. Most importantly, in this situation, allow the customer to vent and assure that their feedback is taken very seriously. "}</p>
                    <p>{"It is important to note that apologies should be sincere - this includes not apologizing when not necessary. Phrases like I am sorry that you are having trouble…' can sound ingenuine and 'script like'. Apologies can also be seen as an admission of guilt, which should not happen when a problem is not the fault of GitLab (such as when a feature works as intended but not the way customer wants)."}</p>
                    <p>{"Escalate to manager if you feel threatened or you think it is required."}</p>
                </details>

                <details>
                    <summary>Responding to negetive/postive feedback from customer</summary>
                    <p>{"One should not take the feedback personally rather, If the customer gives feedback or raises concerns within a ticket, consider reviwing it with your manager. It's important that this feedback is reviewed and acted on. For example, there may be a need in process improvement. "}</p>
                    <p>{"We should remember that customer only gives feedback when they like our service and wanted us to improve. We should treat it as an opportunity to improve. "}</p>
                    <p>{"A positive feedback should be shared within the team for everyone to review and improve."}</p>
                    <p>{"A general rule of thumb appreciate in public reprimand in private."}</p>
                </details>

                <p>
                    <Link href="/" classname="butt">
                        <a>Home</a>
                    </Link>
                </p>
            </section>
        </>
    )
}
