import Link from 'next/link'
import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function CustomerScenario() {
    return (
        <>
            <Head>
                <title>Customer Scenario</title>
            </Head>
            <section>
                <h1>Question 1</h1>
                <details>
                    <summary>  Customer seeking help for redirects</summary>
                    <code>
                        <p>Hi Customer,</p>
                        <p>Thanks for contacting Vercel support</p>
                        <p>To add an in-application redirect, you can use either create a {"'next.config.js'"} configuration file for your projects, or a {"'vercel.json'"} configuration file for all other use cases.</p>
                        <p>You can read more about the redirects in our docs <a className={"'a_link'"} href="https://vercel.com/support/articles/does-vercel-support-permanent-redirects?query=redire#in-application-redirects"> here.</a> </p>
                        <p>Please do let me know in case of any issues.</p>
                        <p>Regards</p>
                    </code>
                </details>
            </section>

            <section>
                <h1>Question 2</h1>
                <details>
                    <summary> Custom domain</summary>
                    <code>
                        <p>Hi Customer,</p>
                        <p>Thanks for contacting Vercel support</p>
                        <p>For settins up a custom domain, you can navigate to Dashboard {">"} YourProject {">"} Settings {">"} domains for setting up a custom domain for your project. Your URL should look like this `https://vercel.com/USERNAME/YOURPROJECTNAME/settings/domains`  </p>
                        <p>You can follow our step by step instructions <a className={"'a_link'"} href="https://vercel.com/docs/concepts/projects/custom-domains#adding-a-domain"> here.</a></p>
                        <p>Please do let us know in case of any issues.</p>
                        <p>Regards</p>
                    </code>
                </details>
                <p>
                    <Link href="/" classname="butt">
                        <a>Back to Home</a>
                    </Link>
                </p>
            </section>

        </>
    )
}

//include both questions
