
import Link from 'next/link'
import Head from 'next/head'

export default function Questions() {
  return (
    <><Head>
      <title>Questions</title>
    </Head>
      <section>
        <h1>
          Most favourite tasks with reasons.
        </h1>
        <details>
          <summary>Logs</summary>
          <p> Dig through logs to troubleshoot a {"customer's"} broken project</p>
          <p> With development background I have a good eye on identifying issues from the logs</p>
        </details>
        <details>
          <summary>Billing Issues</summary>
          <p>
            Help resolve billing issues for customers.
          </p>
          <p>
            I have done a lot of automation in these side of things. Which has resolved 30% of the ticket volume from the total license and renewals tickets queue.
          </p>
        </details>
        <details>
          <summary>Articles and docs</summary>
          <p> Write and maintain support articles and docs pages</p>
        </details>
        <details>
          <summary>Tickets analysis and trends</summary>
          <p> Analyze hundreds of support tickets to spot trends the product team can use</p>
          <p> I prefer following a data driven approach for taking informed decisions. For example we have received 5-10 tickets in a day about a similar issue then I would escalate this to developers and also ask for workaround as this can become a big if not resolved. </p>
        </details>
        <details>
          <summary>Logs</summary>
          <p> Dig through logs to troubleshoot a {"customer's"} broken project</p>
        </details>
        <details>
          <summary>Indentifying bugs and feature requests</summary>
          <p> Identify, file (and, where possible, resolve) bugs in private and public Vercel/Next.js repos on GitHub</p>
        </details>
        <details>
          <summary>Manage a support team</summary>
          <p> Over the period of 6 years, I have learnt a lot about how to build a efficient support team that works globally. I know what challenges one faces while working/managing with such a diverse team working remotely. </p>
        </details>
        <details>
          <summary>Hiring</summary>
          <p> I am very pround on this part of my carrer where I have helped hiring almost 50 support engineers working globally. {"I've"} helped both hiring and sourcing candidates who matches the requirements both functionally and culturally. PS: I have given equal weightage to culture because that matters a lot. </p>
        </details>
        <details>
          <summary>Incident manangement</summary>
          <p> Work with engineering teams during incidents and provide updates to internal and external stakeholders</p>
          <p> I have actively particapted as communication manager on call rotation alongs with customer emegencies rotation. It included passing updates internally to stakeholders and externally to customers across multiple media (e.g.  issues, Twitter, status.gitlab.com, etc.).</p>
        </details>
        <details>
          <summary>Dedicated Support Engineer</summary>
          <p> Act as a dedicated CSE for a handful of key customers to ensure their success using Vercel</p>
          <p>The only issue I have faced with this approach is having a single point of failiure which creates a negative impact on customer. </p>
        </details>
        <details>
          <summary>Community contributions</summary>
          <p> Respond to queries on Twitter, Reddit, Hacker News and other 3rd party sites</p>
          <p> I am active Community contributor on both Stack overflow and {"GitLab's"} community forum </p>
        </details>
        <details>
          <summary>Support coverage</summary>
          <p> Scheduling time-off coverage and collaborating as part of a growing cohesive support team</p>
          <p>I am totally up for covering for team members given that it is fairly scheduled. :)</p>
        </details>
        <details>
          <summary>Customer advocacy, marketing and sales assist</summary>
          <p> Work with people to figure out if Vercel is suitable for their use case</p>
        </details>
      </section>
      <section>
        <h1>
          4 least favourite tasks
        </h1>
        <details>
          <summary>Respond to 50+ support requests via email every day.  </summary>
          <p>For some days or on a high volume day it is fine but I would rather spend an extra hour on spotting the trend and creating documentation about it. So next time team has less tickets regarding that topic. I will also share my findings with the team so they do not have to invest there time in reproducing the problem and forming a reply. I will also create a macro for everyones use.</p>
          <p>I am a firm believer of go slow, to go fast.</p>
        </details>
        <details>
          <summary>Video tutorials</summary>
          <p>Creating a solo video is not my thing. I prefer a collaborative approach or a demo session. I have done this in past while explaining a feature to a colleague and we just recorded our meeting and put that in our docs. It was a great interactive session that covered all probable issues one can face.</p>
        </details>

        <details>
          <summary>Run ticket review sessions to make sure tone is consistent</summary>
          <p>I am not sure what do we mean by tone is consistent? But I prefer having group pairing sessions where team work on tickets together. This will build trust, improve collaboration and guide everyone from writing a good response to great response. This also includes learning new features and identifying bugs.</p>
        </details>
        <details>
          <summary>Public discussions and real time troublehooting</summary>
          <p>I am putting it in a least fav because I am a firm believer of structured approach. Live troubleshooting without knowing the context can backfire.</p>
          <p>If it is a topic that I am comfortable with then yes but I would prefer gathring all the information before jumping on a call. Calls are helpful only when you know what to fix or you know the problem definition. But yeah I agree as sometimes we have to jump into the unknown.</p>
        </details>
        <Link href="/" className="butt">
          <a>Back to home</a>
        </Link>
      </section>
    </>
  )
}