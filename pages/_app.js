import '../styles/globals.css'
import '../styles/questions.css'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
