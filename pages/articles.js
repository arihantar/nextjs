import Link from 'next/link'
import Head from 'next/head'

export default function Article() {
    return (
        <>
            <Head>
                <title>Article</title>
            </Head>
            <section>
                <h1>
                    Books
                </h1>
                <details>
                    <summary>Wings of Fire</summary>
                    <p>An Autobiography of APJ Abdul Kalam, former President of India.</p>
                    <p>This life journey is so inspiring.</p>
                    <p>So many life lessons I have learnt from this book. The most important one is, stay humble.</p>
                </details>
                <details>
                    <summary> The culture code </summary>
                    <p>This is an inspirational read and a call-to-action for any team looking to be more successful. The book takes examples from the best teams on the planet including Pixar, The Navy Seals and the airline industry and draws lessons from these into inspiring but actionable points that can be implemented in your own team. </p>
                    <p>How small changes can have a big impact on the teams.</p>
                </details>

                <details>
                    <summary> Ikigai  </summary>
                    <p>It is a pretty famous one so I will skip this one.</p>
                </details>
            </section>

            <section>
                <h1>
                    Articles and videos
                </h1>
                <details>
                    <summary>Joe Rogan Experience #1309 - Naval Ravikant</summary>
                    <p><a href="https://www.youtube.com/watch?v=3qHkcs3kG44&t=5s">Link</a></p>
                    <p>A better perspective of life.</p>
                    <p>In summary: We have two lives, and the second begins when you realize we only have one.</p>
                    <p>Be adaptive to change.</p>
                </details>

                <details>
                    <summary> Managing a windfall </summary>
                    <p><a href="https://www.bogleheads.org/wiki/Managing_a_windfall">Link</a></p>
                    <p>This article is about managing personal finance.</p>
                    <p>This also talks about how to handle financial losses.</p>
                </details>
                <p>
                    <Link href="/" classname="butt">
                        <a>Home</a>
                    </Link>
                </p>
            </section>
        </>
    )
}
