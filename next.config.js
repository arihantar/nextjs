module.exports = {
  reactStrictMode: true,
  async redirects() {
    return [
      {
        source: '/hello-vercel',
        destination: 'https://vercel.com/',
        permanent: true,
      },
    ]
  },
}
